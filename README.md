# Pipeline deploy on single node K8s 

> How to deploy single node kubernetes cluster via kubeadm and a nginx deployment on it




## General info
This project consists of deploying single node kubernetes cluster via Ansible roles and kubeadm.
The ansible roles are **_installDocker_** and **_installSingleNodek8s_** and you invoke them via a playbook as shown below


```
   ansible-playbook' -b -i ansibleStuffs/ansible_inventory \
   installK8s/install_k8s.yaml -e 'whichNodes=k8snode
   
```

This also uses a Jenkins server to achieve CI/CD and is built with _docker-compose_ and the file is **_jenkinsDocker/docker-compose.yml_**

The Node **_34.217.69.141_** serves as Jenkins 


```
   ubuntu@ip-10-10-0-21:~$ cd jenkins_setup/
   ubuntu@ip-10-10-0-21:~/jenkins_setup$ pwd
   /home/ubuntu/jenkins_setup
   ubuntu@ip-10-10-0-21:~/jenkins_setup$ ls
   Dockerfile  docker-compose.yml  jenkinsvol
   ubuntu@ip-10-10-0-21:~/jenkins_setup$ docker-compose up -d
   ubuntu@ip-10-10-0-21:~/jenkins_setup$

```

The Node **_52.11.245.197_**  has kubernetes installed on it with ingress installed and **_haproxy_** which acts like a external Loadbalancer (which is usually provided by Cloudproviders)
haproxy is installed via ansible playbook **_haproxy_setup.yml_**

```
   ansible-playbook -e "whichNodes=k8snode" haproxy_setup.yml -b \
   -i ansibleStuffs/ansible_inventory

```   

Installing haproxy is a onetime setup which talks to nginx ingress. For each new service you add you could create that many ingress records to expose them outside

## Technologies
Project is created with:

```

- kubernetes: 1.18.0
- ansible: 2.9.10
- haproxy: HA-Proxy version 1.8.8-1ubuntu0.10

```

## Setup
To run the jenkins jobs - **http://34.217.69.141:8081**



```
Pipeline1 - http://34.217.69.141:8081/job/installk8s/ (installs k8s cluster on 52.11.245.197)
Pipeline2 - http://34.217.69.141:8081/job/createKubernetesDeploy/ (deploys nginx app on 52.11.245.197)

```

## Testing

Add the IP address of **52.11.245.197** as **nginxtesting.eqs.com** to your /etc/hosts (for DNS resolution)

```
52.11.245.197 nginxtesting.eqs.com

```

```

curl nginxtesting.eqs.com

```

## Miscellaneous

 Some things were done manually and could be made better. For instance Docker on node **_34.217.69.141_** was installed with the ansible playbook and passing only the **_installDocker_** role.
 kubeconfig was copied to the jenkins secret with the name **kubeconfigfile** and jenkins has its privatekey as another jenkins secret **jenkinsPrivateKey** and it is used to checkoutcode from the bitbucket through ssh
 
 Nginx ingress creation was followed from https://docs.nginx.com/nginx-ingress-controller/installation/installation-with-manifests/ and the steps is documented in **k8sTask/nginx_ingress_install.txt** (could be automated via ansible)

```

- hosts: "{{ whichNodes }}"
  vars:
    kubernetes_allow_pods_on_master: true
  roles:
    - installDocker
    
```    
