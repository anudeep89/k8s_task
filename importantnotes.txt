##to install k8singress
git clone https://github.com/nginxinc/kubernetes-ingress/
git checkout v1.7.2
## to make the ingress run (if issues)
kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission

##create configmaps for nginx
kubectl create configmap default-config --from-file=default.conf
kubectl create configmap nginx-config --from-file=nginx.conf

##To install Ansible
sudo apt update && sudo apt install software-properties-common -y && sudo apt-add-repository --yes --update ppa:ansible/ansible -y && sudo apt install ansible -y

##Add following to end of /etc/haproxy/haproxy.cfg
frontend http_front
  bind *:80
  stats uri /haproxy?stats
  default_backend http_back

backend http_back
  balance roundrobin
  server kube <privateIP>:8080

